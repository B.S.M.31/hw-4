"use strict"

const btn = document.querySelector('.btn')

function setTheme(theme) {
if (theme === 'dark') {
    document.body.classList.add('dark-theme');
} else {
    document.body.classList.remove('dark-theme');
}
}

const savedTheme = localStorage.getItem('theme');
setTheme(savedTheme);

btn.addEventListener("click", () =>{

    const darkTheme = document.body.classList.contains('dark-theme');

    if (darkTheme) {
        setTheme('light');
        localStorage.setItem('theme', 'light');
      } else {
        setTheme('dark');
        localStorage.setItem('theme', 'dark');
      }
})


